# Software Design 2020年1月号 第3章サンプルコード
Software Design 2020年1月号 第3章「自前でGitリポジトリを管理するために知っておかなければならないこと」のサンプルコードです。

## サンプルコードの内容
[Omnibus GitLab](https://docs.gitlab.com/omnibus/) の設定をAnsibleで管理するplaybookのリポジトリです。

このリポジトリは下記のバージョンで動作確認しています

* Python 3.6.3
* Ansible 2.8.5

## このリポジトリを使うための準備
### Ansibleのインストール
Ansibleが必要なので下記手順を参考にインストールしてください。

https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

### サーバ情報の追加
[production](production) にGitLabをインストールしているサーバのIPアドレスを記載してください

### `/etc/gitlab/gitlab.rb` の追加
既存のGitLabサーバに導入する場合はサーバ上にある `/etc/gitlab/gitlab.rb` を [roles/gitlab/templates/etc/gitlab/gitlab.rb.j2](roles/gitlab/templates/etc/gitlab/gitlab.rb.j2) に上書きしてください。

## ファイルの紹介
* [production](production) : playbookを適用するサーバ情報を記載するファイルです
* [site.yml](site.yml) : Ansibleで適用するplaybookファイルです
* [roles/gitlab/](roles/gitlab/) : `site.yml` で適用するroleです。[roles/gitlab/templates/etc/gitlab/gitlab.rb.j2](roles/gitlab/templates/etc/gitlab/gitlab.rb.j2) をサーバにアップロードし、ファイル変更時に自動で `gitlab-ctl reconfigure` を行っています

## playbookの実行方法
最初にdry run実行をして、差分に問題ないことを確認したら本実行をしてください。

### dry run実行
```bash
ansible-playbook --inventory=production site.yml --tags=gitlab --diff --check
```

### 本実行
```bash
ansible-playbook --inventory=production site.yml --tags=gitlab --diff
```

## 付録
本誌に書ききれなかった記事を付録として下記エントリで公開しています。

https://sue445.hatenablog.com/entry/2019/12/18/000108
